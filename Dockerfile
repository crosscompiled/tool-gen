FROM debian:10-slim
RUN apt-get update &&\
 apt-get upgrade -y &&\
 apt-get install build-essential autoconf libtool git wget bzip2 -y &&\
 rm -rf /var/lib/apt/lists/*
ARG toolchain
ENV toolchain=${toolchain}
RUN wget -q -O /tool.tar.bz2 "https://toolchains.bootlin.com/downloads/releases/toolchains/$(echo ${toolchain} | grep -ioP '^.+?(?=--)')/tarballs/${toolchain}.tar.bz2" &&\
 mkdir /tool &&\
 cd /tool &&\
 tar --strip-components=1 -xf /tool.tar.bz2 &&\
 rm /tool.tar.bz2

ENV PATH="${PATH}:/tool/bin"

ENTRYPOINT [ "/bin/bash" ]
