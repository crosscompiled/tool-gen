#!/bin/bash

toolchains=$(<toolchains-latest)
for tool in ${toolchains[@]}; do
  ./tool-gen.sh $tool
done
