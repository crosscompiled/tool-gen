#!/bin/bash

arches=$(curl -s https://toolchains.bootlin.com/downloads/releases/toolchains/  | grep -ioE 'href="([^"?\/]+)\/"' | cut -c6- | tr -d '"'| tr -d '/')
for arch in ${arches[@]}; do
    toolchains=$(curl -s https://toolchains.bootlin.com/downloads/releases/toolchains/$arch/available_toolchains/ | grep -ioE 'href="([^"?\/]+)"' | cut -c6- | tr -d '"' )
    for tool in ${toolchains[@]}; do
      echo $tool >> toolchains
    done
done