#!/bin/bash

docker build --build-arg toolchain=$1 -t quay.io/crosscompiled/tool:bootlin--$1 .
docker push quay.io/crosscompiled/tool:bootlin--$1