#!/bin/bash

toolchains=$(<toolchains)
for tool in ${toolchains[@]}; do
  ./tool-gen.sh $tool
done
